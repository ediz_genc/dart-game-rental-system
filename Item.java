import java.time.LocalDateTime;
import java.util.ArrayList;

public abstract class Item {

    private int id;
    private String title;
    private double rent;
    private String status;
    private double rating;
    private int numOfRating;
    private ArrayList<String> itemReviews;

    public Item(int id, String title, double rent, String status, double rating) throws Exception{
        if(title.isEmpty()){
            throw new Exception("Invalid data. Item name cannot be empty.");
        }
        if(rent < 0){
            throw new Exception("Item daily rent fee cannot be negative.");
        }

        this.id = id;
        this.title = title;
        this.rent = rent;
        this.status = status;
        this.rating = rating;
        this.numOfRating = 0;
        this.itemReviews = new ArrayList<>();

    }

    public Item() {

    }

    public int getId () {
        return id;
    }

    public void setId ( int aid){
        this.id = aid;
    }

    public String getTitle () {
        return title;
    }

    public void setTitle (String title){
        this.title = title;
    }

    public double getRent () {
        return rent;
    }

    public void setRent ( double rent){
        this.rent = rent;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status){
        this.status = status;
    }

    public double getRating () {
        return rating;
    }

    public void setRating ( double rating){
        this.rating = rating;
    }

    public int getNumOfRating() {
        return numOfRating;
    }

    public void setNumOfRating(int numOfRating) {
        this.numOfRating = numOfRating;
    }

    public ArrayList<String> getItemReviews() {
        return itemReviews;
    }

    public void setItemReviews(ArrayList<String> itemReviews){
        this.itemReviews = itemReviews;
    }


    public abstract ArrayList<String> updateItemReview(ArrayList<String> itemReviews, String itemReview);

    public abstract double getAvgRating(Item item, double rating, int counter);

    public abstract void setTime(LocalDateTime timeNow);
}


