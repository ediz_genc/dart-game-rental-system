import java.util.ArrayList;

public class SilverMember extends Member {

    final int rentLimit = 3;
    final double discount = 0.9;
    final int plusCredits = 1;

    public SilverMember (int customerId, String customerName){
        super(customerId, customerName);
    }

    public double calRentCost(int days, double rent) {
        double rentCost;

        if (getCredits() < subCredits) {
            rentCost = ((rent * days) * discount);
        }else{
            setCredits(getCredits() - subCredits);
            rentCost = 0.0;
        }
        return rentCost;
    }

    public ArrayList<Customer> approvedUpgrade(ArrayList<Customer> allCustomers) {

        int id = this.getCustomerId();
        String name = this.getCustomerName();
        GoldMember newMember = new GoldMember(id, name);
        newMember.setCredits(this.getCredits());
        newMember.setItemNo(this.getItemNo());
        allCustomers.remove(this);
        allCustomers.add(newMember);
        return allCustomers;
    }
}
