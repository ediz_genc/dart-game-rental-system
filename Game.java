import java.time.LocalDateTime;
import java.util.ArrayList;

public class Game extends Item {

    private LocalDateTime time;
    private String genre;

    public Game(int id, String title, double rent, String status, double rating, String genre) throws Exception{
        super(id, title, rent, status, rating);
        if(title.isEmpty()){
            throw new Exception("Invalid data. Game name cannot be empty.");
        }
        if(rent < 0){
            throw new Exception("Game daily rent fee cannot be negative.");
        }

        this.genre = genre;

    }

    public Game() {

    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String toString() {
        return "<" + this.getId() + "> : <" + this.getTitle() + ">. Genre: <" + this.getGenre() + ">. Price: <" + this.getRent() + "> SEK. Status: <" + this.getStatus() + ">. " +
                " Average Rating: " + String.format("%.2f", super.getRating()) + " , Review: " + super.getItemReviews();
    } // (Ediz) Source of decimal formatting "String.format(%.2f") : https://www.codegrepper.com/code-examples/delphi/how+to+print+only+2+decimal+places+in+java


    public ArrayList<String> updateItemReview (ArrayList < String > gameReviews, String gameReview){
        gameReviews.add(gameReview);
        return gameReviews;
    }

    public double getAvgRating(Item game, double rating, int counter) {
        double average = (game.getRating() + rating) / counter;
        setRating(average);
        return average;
    }
}