import java.time.LocalDateTime;
import java.util.ArrayList;

public class Album extends Item{

    private String songArtist;
    private int songReleaseYear;

    public Album(int id, String title, double rent, String status, double rating, String songArtist, int songReleaseYear)throws Exception{
        super(id, title, rent, status, rating);
        if(title.isEmpty()){
            throw new Exception("Invalid data. Album name cannot be empty.");
        }
        if(rent < 0){
            throw new Exception("Album daily rent fee cannot be negative.");
        }

        this.songArtist = songArtist;
        this.songReleaseYear = songReleaseYear;

    }

    public Album(){
    }

    public String getSongArtist() {
        return songArtist;
    }

    public void setSongArtist(String songArtist) {
        this.songArtist = songArtist;
    }

    public int getSongReleaseYear() {
        return songReleaseYear;
    }

    public void setSongReleaseYear(int songReleaseYear) {
        this.songReleaseYear = songReleaseYear;
    }

    @Override
    public void setTime(LocalDateTime timeNow) {

    }

    public String toString(){
        return "<" + this.getId() + "> : <" + this.getTitle() + "> - by <" + this.getSongArtist() + ">. " +
                "Released in <" + this.getSongReleaseYear() + ">. Price: <" + this.getRent() + "> SEK. Status: <" + this.getStatus() + ">." +
                " Average Rating: " + String.format("%.2f", super.getRating()) + " , Review: " + super.getItemReviews();
    }// (Ediz) Source of decimal formatting "String.format(%.2f") : https://www.codegrepper.com/code-examples/delphi/how+to+print+only+2+decimal+places+in+java

    public ArrayList<String> updateItemReview (ArrayList <String> albumReviews, String albumReview){
        albumReviews.add(albumReview);
        return albumReviews;
    }

    public double getAvgRating (Item album, double rating, int counter){
        double average = (album.getRating() + rating) / counter;
        setRating(average);
        return average;
    }
}
