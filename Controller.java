import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;

// To implement Epic Feature 5.1 which includes the important controller abstraction, I created the class "Controller" by
// adding all the methods in "Epic future 2,3,4,5,6,7,8,9,10,11 and 12". (Ediz Genc)
public class Controller {

    // Source (Ediz): https://stackoverflow.com/questions/5762491/how-to-print-color-in-console-using-system-out-println
    public final String ANSI_RED = "\u001B[31m";
    public final String ANSI_RESET = "\u001B[0m";

    //add lists here
    private ArrayList<Employee> allEmployees;
    private ArrayList<Customer> allCustomers;
    private ArrayList<Item> listOfItems;
    private ArrayList<Messages> allMessages;
    private ArrayList<RentTransaction> allTransactions;
    private ArrayList<Customer> listOfUpgrades;

    private Utility utility;
    private Screens screens;

    public Controller() {
        this.allEmployees = new ArrayList<>();
        this.allCustomers = new ArrayList<>();
        this.listOfItems = new ArrayList<>();
        this.allMessages = new ArrayList<>();
        this.allTransactions = new ArrayList<>();
        this.listOfUpgrades = new ArrayList<>();
        this.utility = new Utility();
        this.screens = new Screens();
    }

    public ArrayList<Employee> getAllEmployees() {
        return allEmployees;
    }

    public void setEmployeeList(ArrayList<Employee> employeeList) {
        this.allEmployees = employeeList;
    }

    public ArrayList<Customer> getAllCustomers() {
        return allCustomers;
    }

    public void setCustomerList(ArrayList<Customer> customerList) {
        this.allCustomers = customerList;
    }

    public ArrayList<Item> getListOfItems() {
        return listOfItems;
    }

    public void setListOfItems(ArrayList<Item> listOfItems) {
        this.listOfItems = listOfItems;
    }

    public ArrayList<Messages> getAllMessages() {
        return allMessages;
    }

    public void setAllMessages(ArrayList<Messages> allMessages) {
        this.allMessages = allMessages;
    }

    public ArrayList<RentTransaction> getAllTransactions() {
        return allTransactions;
    }

    public void setAllTransactions(ArrayList<RentTransaction> allTransactions) {
        this.allTransactions = allTransactions;
    }

    public ArrayList<Customer> getListOfUpgrades() {
        return listOfUpgrades;
    }

    public void setListOfUpgrades(ArrayList<Customer> listOfUpgrades) {
        this.listOfUpgrades = listOfUpgrades;
    }

    public Utility getInputClass() {
        return utility;
    }

    public void setInputClass(Utility utility) {
        this.utility = utility;
    }

    public Screens getScreens() {
        return screens;
    }

    public void setScreens(Screens screens) {
        this.screens = screens;
    }


    //  “To implement Epic feature 12.1 as one of the feature mentioned in Milestone 3,
//  I modified the class "controller" and methods "managerIsLoggedIn", "addEmployee" and "constructor in employee class
//  by adding "try&catch block" in managerIsLoggedIn & "throw Exception" in constructor in employee class. (Ediz Genc)
    public void managerIsLoggedIn(boolean access){
        if (access) {
            Manager manager = new Manager();
            int managerScreenMenu;

            do {
                screens.managerScreen();
                managerScreenMenu = utility.readInt("Your option is: ");

                switch (managerScreenMenu) {
                    case 1:
                        System.out.println("Creating an Employee. Please type the Employee's ID: ");
                        int employeeId = utility.generateId();
                        System.out.println("ID: " + employeeId);
                        try{
                            String employeeName = utility.readString("Name: ");
                            int employeeBirthYear = utility.readInt("Birth year: ");
                            String employeeAddress = utility.readString("Address: ");
                            double employeeGrossSalary = utility.readDouble("Monthly gross salary: ");
                            setEmployeeList(manager.addEmployee(allEmployees, employeeId, employeeName, employeeBirthYear, employeeAddress, employeeGrossSalary));
                        } catch(Exception exception){
                            System.out.println();
                            System.out.println(ANSI_RED + exception.getMessage() + ANSI_RESET);
                            System.out.println();
                        }
                        break;
                    case 2:     manager.viewEmployees(allEmployees);
                                int removeElement = utility.readInt("Which employee should be removed? ID: ");
                                manager.removeEmployee(allEmployees, removeElement);
                        break;
                    case 3:     manager.employeeSalary(allEmployees);
                        break;
                    case 4:     manager.viewEmployees(allEmployees);
                        break;

                    // “To implement Epic Feature 11, I modified the class Controller by
                    // adding cases to manager screen to call the methods getTotalProfit, getBestCustomer,
                    // getRentFrequency and getHighestProfitItem" (Patricia)

                    case 5:     manager.getHighestProfitItem(allTransactions);
                        break;
                    case 6:     manager.getItemRentFrequency(allTransactions);
                        break;
                    case 7:     manager.getBestCustomer(allTransactions);
                        break;
                    case 8:     manager.getTotalRentProfit(allTransactions);
                        break;
                    case 9:     manager.importFiles(allEmployees, allCustomers,listOfItems);
                        break;
                    case 10:
                        break;
                    default: {
                        System.out.println("Invalid input. Please try again.");
                        break;
                    }
                }
            } while (managerScreenMenu != 10);
        } else {
            System.out.println("Invalid password, try again");
        }
    }

//  “To implement Epic feature 12.2 as one of the feature mentioned in Milestone 3,
//  I modified the class "controller" and methods "employeeIsLoggedIn", "addGame", "addAlbum" and "constructors in abstract class item and
//  game & album classes by adding "try&catch block" in employeeIsLoggedIn &
//  "throw Exception" in constructors in item, game and album classes. (Ediz Genc)
    public void employeeIsLoggedIn(boolean access) throws Exception {

        if (access) {
            Employee employee = new Employee();
            int employeeScreenMenu;

            do {
                screens.employeeScreen();
                employeeScreenMenu = utility.readInt("Your option is: ");

                switch (employeeScreenMenu) {
                    case 1:
                        int id = utility.generateId();
                        System.out.println("ID : " + id);
                        try{
                            String title = utility.readString("Title: ");
                            String genre = utility.readString("Genre: ");
                            double rent = utility.readDouble("Daily rent fee: ");
                            setListOfItems(employee.addGame(listOfItems, id, title, rent, genre));
                        }catch(Exception exception){
                            System.out.println();
                            System.out.println(ANSI_RED + exception.getMessage() + ANSI_RESET);
                            System.out.println();
                        }
                        break;
                    case 2:
                        employee.viewAllGames(listOfItems);
                        int gameToRemove = utility.readInt("Which game should be removed? ID: ");
                        employee.removeGame(listOfItems, gameToRemove);
                        break;
                    case 3:
                        System.out.println("Creating a Customer. Please type the customer’s: ");

                        int customerId = utility.generateId();
                        System.out.println("ID: " + customerId);
                        String customerName = utility.readString("Name:  ");
                        setCustomerList(employee.addCustomers(allCustomers, customerId, customerName));
                        break;
                    case 4:
                        employee.viewCustomers(allCustomers);
                        int customerIdToRemove = utility.readInt("Which customer should be removed? ID: ");
                        employee.removeCustomer(allCustomers, customerIdToRemove);
                        break;
                    case 5:
                        int albumId = utility.generateId();
                        System.out.println("ID: " + albumId);
                        try{
                            String albumTitle = utility.readString("Please enter the title: ");
                            String songArtist = utility.readString("Please enter the artist: ");
                            int songReleaseYear = utility.readInt("Please enter the album release year: ");
                            double dailyRent = utility.readDouble("Please enter the album daily price: ");
                            setListOfItems(employee.addAlbum(listOfItems, albumId, albumTitle, dailyRent, songArtist,songReleaseYear));
                        }catch(Exception exception){
                            System.out.println();
                            System.out.println(ANSI_RED + exception.getMessage() + ANSI_RESET);
                            System.out.println();
                        }
                        break;
                    case 6:
                                employee.viewAlbums(listOfItems);
                                int songToRemove = utility.readInt("Which album should be removed? ID:  ");
                                employee.removeAlbum(listOfItems, songToRemove);
                        break;
                    case 7:     employee.viewAllGames(listOfItems);
                        break;
                    case 8:     employee.viewCustomers(allCustomers);
                        break;
                    case 9:     employee.viewAlbums(listOfItems);
                        break;
                    case 10:    upgradeMember(); //8.2
                        for (int i=0; i < listOfUpgrades.size(); i++) {
                            if (listOfUpgrades.get(i).getGrantedUpgrade() == true) {
                                listOfUpgrades.get(i).approvedUpgrade(allCustomers);
                                listOfUpgrades.remove(listOfUpgrades.get(i));
                            }
                        }
                        break;
                    case 11:
                        break;
                    default: {
                        System.out.println("Invalid input. Please try again.");
                        break;
                    }
                }
            } while (employeeScreenMenu != 11);
        } else {
            System.out.println("Invalid password, try again");
        }
    }


    // “I modified the class Controller by
    // Removing user input from the method rentItem and return item, and adding it to the controller and
    // sending it back to the method.” (Patricia)
    public void costumerIsLoggedIn(int customerId){
        //customerIsLoggedIn
        Customer customer = new Customer();
        int customerScreenMenu;
        for (Customer customerLoop : allCustomers) {
            if (customerId == customerLoop.getCustomerId()){
                customer = customerLoop;
            }
        }

        do {
            screens.customerScreen();
            customerScreenMenu = utility.readInt("Your option is: ");

            switch (customerScreenMenu) {
                case 1:
                    customer.viewAllItems(listOfItems);
                    int itemChoice = utility.readInt("Which item do you want to rent?: ");
                    customer.rentItem(listOfItems, itemChoice);
                    break;
                case 2:
                    customer.viewAllItems(listOfItems);
                    System.out.println("Which item do you want to return?");
                    int itemId = utility.readInt("Please type the item's ID: ");
                    Controller controller = new Controller();
                    LocalDateTime todayDate = LocalDateTime.now();
                    try{
                        int itemDaysRented = utility.readInt("Type the number of days it was rented: ");
                        customer.returnItem(listOfItems, allTransactions, itemId, itemDaysRented);
                    }catch(Exception exception){
                        System.out.println();
                        System.out.println(controller.ANSI_RED + exception.getMessage() + controller.ANSI_RESET);
                        System.out.println();
                    }
                    break;
                case 3:
                    int senderId;
                    boolean senderExist = false;
                    do{
                        senderId = utility.readInt("Enter your id: ");
                        for(int i = 0; i < allCustomers.size(); i++){
                            if(allCustomers.get(i).getCustomerId() == senderId){
                                senderExist = true;
                            }
                        }
                    }while(senderExist == false);
                    screens.printCustomers(allCustomers);

                    int idToSendTo = utility.readInt("Select an ID to send a message to: ");
                    String newMessage = utility.readString("Message: ");

                    customer.sendMessage(allCustomers, allMessages, senderId, idToSendTo, newMessage);
                    break;
                case 4:
                    int yourID = utility.readInt("Please enter your id: ");
                    screens.printReceivedMessages(allMessages, yourID);
                    int option = utility.readInt("1. View all messages   2. Return to customer menu");
                    if (option == 1){
                        screens.printAllMessages(allMessages, yourID);
                    }
                    break;

                   // customer.receivedMessages(allMessages);

                case 5:
                    yourID = utility.readInt("Enter your id: ");
                    screens.sentMessages(allMessages, yourID);
                    break;
                case 6:     searchAnItem(listOfItems);
                    break;
                case 7:     customer.viewSortedBasedOnYear(listOfItems);
                    break;
                case 8:     viewSortedBasedOnRating(listOfItems);
                    break;
                case 9:     changePassword(allCustomers);
                    break;
                case 10:    customer.wantsUpgrade(listOfUpgrades);  //Epic 8.2
                    break;
                case 11:
                    break;
                default:
                    System.out.println("Invalid input. Please try again.");
                    break;
            }
        } while (customerScreenMenu != 11);
    }

//  “To implement customer password as one of the feature mentioned in Milestone 2, I modified the class "controller" by
//  adding the attributes "customerId" and "customerPassword" in the main method in dartMain class and the method
//  itself "verifyCustomer". (Ediz Genc)
    public boolean verifyCustomer(int customerId, String customerPassword){

        for(Customer customer: allCustomers) {
            if (customer.getCustomerId() == customerId) {
                System.out.println();
                if (customer.getPassword().equals(customerPassword)) {
                    return true;
                }
            }
        }
        return false;
    }

//  “To implement customer to have an unique password as one of the feature mentioned in Milestone 2,
//  I modified the class "customer" by adding the attributes "oldPassword", ""newPassword" & "oonfirmPassword" and
//  in the method and the method itself "changePassword" in customer class. (Ediz Genc)
    public boolean changePassword(ArrayList<Customer> allCustomer) {
        String oldPassword = utility.readString("Type the old password: ");
        for (int i = 0; i < allCustomer.size(); i++) {
            if (oldPassword.equals(allCustomer.get(i).getPassword())) {
                String newPassword = utility.readString("Type the new password: ");
                String confirmNewPassword = utility.readString("Confirm the new password: ");
                if (newPassword.equals(confirmNewPassword)) {
                    allCustomer.get(i).setPassword(newPassword);
                } else {
                    System.out.println("Passwords do not match! Please type again!");
                }
                return true;
            }
        }
        return false;
    }

//  “To implement Epic Feature 10.1, I modified the class "customer" by
//  adding the attributes "rate", "review" and methods "rate", "review" in customer class and
//  abstract methods "updateItemReview", "getAveRating" in abstract item class by overriding in game and album classes. (Ediz Genc)

    public Item rate(Item returnedItem){
        if (returnedItem != null) {
            System.out.println();
            System.out.println("1.Very Poor / 2.Poor / 3.Fair / 4.Good / 5.Excellent");
            double rating = utility.readDouble("How would you rate this experience?: ");
            if (rating > 0 && rating <= 5) {
                returnedItem.setNumOfRating(returnedItem.getNumOfRating() + 1);
                int counter = returnedItem.getNumOfRating();
                returnedItem.getAvgRating(returnedItem, rating, counter);
                System.out.println("Your rating \"" + rating + "\" registered successfully.");
                System.out.println("Thank you for the rating!");
            } else {
                System.out.println("Invalid rating! Please rate from 1 to 5!");
            }
        }
        review(returnedItem);
        return returnedItem;
    }

    //  “To implement Epic Feature 10.1, I modified the class "customer" by
//  adding the attributes "rate", "review" and methods "rate", "review" in customer class and
//  abstract methods "updateItemReview", "getAveRating" in abstract item class by overriding in game and album classes. (Ediz Genc)
    public Item review(Item returnedItem){
        if (returnedItem != null) {
            String review = utility.readString("Write a review about your experience: ");
            System.out.println("Your review \"" + review + "\" registered successfully.");
            System.out.println("Thank you for the review!");
            returnedItem.updateItemReview(returnedItem.getItemReviews(), review);
        }
        return returnedItem;
    }

    //  “To implement Epic Feature 10.2, I modified the class "customer" by
//  adding the attributes "genreGame" and "yearAlbum" in methods and the method itself "searchAnItem" in customer class. (Ediz Genc)
    public void searchAnItem(ArrayList<Item> listOfItems) {
        System.out.println();
        char searchSelection = utility.readChar("Would you like to search through [G]ames or [A]lbums?");
        utility.readString("");
        searchSelection = Character.toUpperCase(searchSelection);
        switch (searchSelection) {
            case 'G':
                ArrayList<Game> listOfGames = new ArrayList<>();
                for(Item itemGame :listOfItems) {
                    if(itemGame instanceof Game){
                        listOfGames.add((Game)itemGame);
                    }
                }
                String genreGame = utility.readString("What genre would you like to use for searching in games?: ");
                for (int i = 0; i < listOfItems.size(); i++) {
                    if(listOfItems.get(i) instanceof Game) {
                        if (((Game)listOfItems.get(i)).getGenre().contains(genreGame)) {
                            System.out.println((listOfItems.get(i)));
                        }
                    }
                }
                break;
            case 'A':
                ArrayList<Album> listOfSongs = new ArrayList<>();
                for(Item itemAlbum :listOfItems) {
                    if(itemAlbum instanceof Album){
                        listOfSongs.add((Album)itemAlbum);
                    }
                }
                int yearAlbum = utility.readInt("Which year would you like to use for searching in albums?: ");
                for(int i = 0; i < listOfItems.size(); i++){
                    if(listOfItems.get(i) instanceof Album) {
                        if (((Album)listOfItems.get(i)).getSongReleaseYear() == yearAlbum) {
                            System.out.println(listOfItems.get(i));
                        }
                    }
                }
                break;
            default:
                System.out.println("Invalid value. Please try again!");
                break;
        }
    }

    //  “To implement Epic Feature 10.3, I modified the class "customer" by
//  adding the attributes "sortGame" and "sortAlbum" in methods and the methods "viewSortedBasedOnRating",
//  "sortGame" and "sortAlbum" in customer class. (Ediz Genc)
    public void viewSortedBasedOnRating(ArrayList<Item> listOfItems) {
        Customer customer = new Customer();
        System.out.println();
        char sortSelection = utility.readChar("Would you like to sort through [G]ames or [A]lbums?");
        sortSelection = Character.toUpperCase(sortSelection);
        switch (sortSelection) {
            case 'G':
                ArrayList<Game> listOfGames = new ArrayList<>();
                for(Item item :listOfItems) {
                    if(item instanceof Game){
                        listOfGames.add((Game)item);
                    }
                }
                customer.sortGame(listOfGames);
                break;
            case 'A':
                ArrayList<Album> listOfSongs = new ArrayList<>();
                for(Item item :listOfItems) {
                    if(item instanceof Album){
                        listOfSongs.add((Album)item);
                    }
                }
                customer.sortAlbum(listOfSongs);
                break;
            default:
                System.out.println("Invalid value. Please try again!");
                break;
        }
    }

    public void upgradeMember() { //8.2
        String answer;
        if (listOfUpgrades.size() > 0) {

            do {
                for(int i=0; i<listOfUpgrades.size(); i++) {
                    System.out.println("index: " + i + " - " + listOfUpgrades.get(i));
                }
                answer = utility.readString("Do you want to apply any upgrade?");

                if (answer.equalsIgnoreCase("yes")){
                    int indexUpgrade = utility.readInt("What is the index of the customer you want to upgrade?");
                    listOfUpgrades.get(indexUpgrade).setGrantedUpgrade(true);
                }

            }while (answer.equalsIgnoreCase("yes"));

        }else {
            System.out.println("There is no customer that has applied for an upgrade.");
        }
        return;
    }

}