import java.util.ArrayList;

public class GoldMember extends Member {

    final int rentLimit = 5;
    final double discount = 0.85;
    final int plusCredits = 3;

    public GoldMember (int customerId, String customerName){
        super(customerId, customerName);
    }

    public double calRentCost(int days, double rent) {

        double rentCost;

        if (this.getCredits() < subCredits) {
            rentCost = ((rent * days) * discount);
        }else{
            this.setCredits( getCredits() - subCredits);
            rentCost = 0.0;
        }
        return rentCost;
    }

    public ArrayList<Customer> approvedUpgrade(ArrayList<Customer> allCustomers) {

        int id = super.getCustomerId();
        String name = super.getCustomerName();

        PremiumMember prem  = new PremiumMember(id, name);
        prem.setCredits(this.getCredits());
        prem.setItemNo(this.getItemNo());
        allCustomers.remove(this);
        allCustomers.add(prem);
        return allCustomers;
    }
}
