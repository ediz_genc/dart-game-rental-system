
public abstract class Member extends Customer {

    private int credits;
    private int itemNo;
    final int subCredits = 5;
    final int plusCredits = 1;

    public Member (int customerId, String customerName) {
        super(customerId, customerName);
        this.itemNo = 0;
        this.credits = 0;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public int getCredits(){
        return this.credits;
    }

    public void plusCredit(){
        this.credits = this.credits + plusCredits;
    }
}
