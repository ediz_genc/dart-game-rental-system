import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class Manager {
    final double taxThreshold = 100000.0;
    final double taxRate = 0.7;
    final int monthsInAYear = 12;
    final double ageYoung = 23;
    final double bonusAmountYoung = 4000.0;
    final int ageMiddle = 30;
    final double bonusAmountYoungToMiddle = 6000.0;
    final double bonusAmountOlder = 7500;

    //“To implement Epic Feature 2.1, I modified the class "Manager" by
    // adding methods addEmployee and removeEmployee." (Ediz Genc)
    public ArrayList<Employee> addEmployee(ArrayList<Employee> allEmployees, int employeeId, String employeeName, int employeeBirthYear,  String employeeAddress, double employeeGrossSalary) throws Exception {

        Employee employee = new Employee(employeeId, employeeName, employeeBirthYear, employeeAddress, employeeGrossSalary);
        allEmployees.add(employee);

        System.out.println();
        return allEmployees;
    }

    //“To implement Epic Feature 2.1, I modified the class "Manager" by
    // adding methods addEmployee and removeEmployee." (Ediz Genc)
    public void removeEmployee(ArrayList<Employee> allEmployees, int removeElement) {

        int counter = 0;
        for(int i = 0; i < allEmployees.size(); i ++){
            if(allEmployees.get(i).getEmployeeId() == removeElement) {
                allEmployees.remove(i);
                System.out.println("Employee with ID " + removeElement + " removed successfully. ");
                System.out.println("The adjusted employee list is as follows; ");
                if(allEmployees.size() > 0){
                    for (Employee listOfEmployees: allEmployees) {
                        System.out.println(listOfEmployees);
                    }
                }else if (allEmployees.size() == 0){
                    System.out.println("There are no registered employees.");
                }else{
                    System.out.println("All employees have been successfully removed.");
                }
                System.out.println();
            }else{
                if (counter < 1) {
                    System.out.println("Employee with ID " + removeElement + " not found!");
                    counter++;
                }
            }
        }
        System.out.println();
    }

    public void viewEmployees(ArrayList<Employee> allEmployees) {
        System.out.println("Current employees are as follows; ");
            if (allEmployees.size() > 0){
                for (Employee listOfEmployees: allEmployees) {
                    System.out.println(listOfEmployees);
                }
            }else{
                System.out.println("There are no registered employees.");
            }
        System.out.println();
    }

    public void employeeSalary(ArrayList<Employee> allEmployees) {   //EF 2.3 - Employee Salary
        System.out.println("-----Total Employee Costs Yearly-----");
        double totYearlySalaryExpenses = 0.0;
        for (Employee em : allEmployees) {
            if ((em.getEmployeeGrossSalary() * monthsInAYear) >= taxThreshold) {  //individual employee salary(yearly) more than 100k
                totYearlySalaryExpenses = totYearlySalaryExpenses + ((em.getEmployeeGrossSalary() * taxRate) * monthsInAYear) /* + (bonus = salaryBonus(em.getAge()))*/;
            } else if ((em.getEmployeeGrossSalary() * monthsInAYear) < taxThreshold) {   //individual employee salary(yearly) less than 100k
                totYearlySalaryExpenses = totYearlySalaryExpenses + (em.getEmployeeGrossSalary() * monthsInAYear) /*+ salaryBonus(em.getAge())*/;
            }
        }
        totYearlySalaryExpenses = totYearlySalaryExpenses + salaryBonus(allEmployees);
        System.out.println("Total employee costs per year: " + String.format("%.2f", totYearlySalaryExpenses));
        // Source of decimal formatting "%.2f": https://www.codegrepper.com/code-examples/delphi/how+to+print+only+2+decimal+places+in+java
    }

    public double salaryBonus(ArrayList<Employee> allEmployees) {
        double totSalaryBonus = 0.0;
        for (Employee employee : allEmployees) { //EF 2.4 - Salary bonus
            if (employee.getAge() < ageYoung) {
                totSalaryBonus = totSalaryBonus + bonusAmountYoung;
            } else if (employee.getAge() >= ageYoung && employee.getAge() <= bonusAmountYoungToMiddle) {
                totSalaryBonus = totSalaryBonus + bonusAmountYoungToMiddle;
            } else if (employee.getAge() > ageMiddle) {
                totSalaryBonus = totSalaryBonus + bonusAmountOlder;
            }
        }
        return totSalaryBonus;
    }

    // “To implement Epic Feature 11, I modified the class Manager by removing methods getHighestProfitItem,
    // getItemRentFrequency and getBestCustomer from RentTransaction to Manager Class.
    // adding the methods countFrequency and countBestCustomer to properly make the calculation” (Patricia)
    public double getTotalRentProfit (ArrayList <RentTransaction> allTransactions) {
        double profit = 0;
        for (int i = 0; i < allTransactions.size(); i++) {
            profit = profit + allTransactions.get(i).getTotalRentCost();
        }

        System.out.println("Your profit for the rent of games and songs are: "+ profit);
        return profit;
    }

    //Calculating item with highest profit
    public ArrayList <RentTransaction> getHighestProfitItem (ArrayList<RentTransaction> allTransactions) {
        int indexStart = 0;
        int mostProfitableItem = 0;
        for (int i = 0; i < allTransactions.size(); i++) {
            if (allTransactions.get(indexStart).getTotalRentCost() > allTransactions.get(i).getTotalRentCost()) {
                mostProfitableItem = allTransactions.get(indexStart).getItemId();
            }else {
                mostProfitableItem = allTransactions.get(i).getItemId();
            }
        }
        System.out.println(" ");
        System.out.println("The item with highest profit is: "+ mostProfitableItem + " SEK");
        System.out.println(" ");
        return allTransactions;
    }

    //New method added. Patricia
    private int countFrequency(int id, ArrayList<RentTransaction> allTransactions) {
        int frequency = 0;
        for(int i = 0;i<allTransactions.size();i++) {
            if(allTransactions.get(i).getItemId() == id) {
                frequency++;
            }
        }
        return frequency;
    }
    //Method to search for frequency an item has been rented. Patricia
    public ArrayList <RentTransaction> getItemRentFrequency(ArrayList<RentTransaction> allTransactions) {
        ArrayList <Integer> uniqueIDs = new ArrayList<Integer>();
        for(int j = 0; j < allTransactions.size(); j++) {
            if(!uniqueIDs.contains(allTransactions.get(j).getItemId())) {
                uniqueIDs.add(allTransactions.get(j).getItemId());
            }
        }
        System.out.println("The following items were rented: " );
        for (int i = 0; i < uniqueIDs.size(); i++) {
            if (countFrequency(uniqueIDs.get(i), allTransactions) > 0) {
                System.out.println("Item: " + uniqueIDs.get(i) + " was rented for: " + countFrequency(uniqueIDs.get(i), allTransactions) + " times");
            }
        }
        System.out.println(" ");
        return allTransactions;
    }

    private int countBestCustomer(double totalRentCost, ArrayList<RentTransaction> allTransactions) {
        int amountOfRents = 0;
        for(int i = 0;i<allTransactions.size();i++) {
            if(allTransactions.get(i).getItemId() == totalRentCost) {
                amountOfRents++;
            }
        }
        return amountOfRents;
    }

    public ArrayList<RentTransaction> getBestCustomer (ArrayList<RentTransaction> allTransactions) {
        int bestCustomer = 0;
        for (RentTransaction rt: allTransactions) {
            System.out.println(rt);
        }
        for (int i = 0; i < allTransactions.size(); i++) {
            if (countBestCustomer(allTransactions.get(i).getTotalRentCost(), allTransactions) > countBestCustomer(bestCustomer, allTransactions));
            bestCustomer = allTransactions.get(i).getCustomerId();
        }
        System.out.println("The best customer is: "+ bestCustomer);
        return allTransactions;
    }

    public void importFiles(ArrayList<Employee> allEmployees, ArrayList<Customer> allCustomers, ArrayList<Item> listOfItems){
        System.out.println("-----Import File-----");
        try {
            // File fileToImport = new File("C:\\Users\\robin\\Desktop\\programming\\projects\\java\\DART\\importFile.txt");

            File fileToImport = new File(".\\importFile.txt");
            if (fileToImport.createNewFile()){      //^^ Dynamic file path. Learned from: https://stackoverflow.com/questions/3844307/how-to-read-file-from-relative-path-in-java-project-java-io-file-cannot-find-th
                //^^by: santhosh    &   Mohamed Taher Alrefaie  (2020-10-20:17.12)
                System.out.println("Created new file with name: " + fileToImport.getName() + "\nat: " + fileToImport.getAbsolutePath());

                importFiles(allEmployees, allCustomers, listOfItems);
            } else{
                System.out.println("Import file("+ fileToImport.getName() + ") found at: " + fileToImport.getAbsolutePath());
                Scanner readFile = new Scanner(fileToImport);
                for (int i = 0; i < fileToImport.length(); i++) {
                    try {
                        String wholeLine = readFile.nextLine();
                        String[] onePart = wholeLine.split(";");   //Used site to learn string.split("-"); https://stackoverflow.com/questions/3481828/how-to-split-a-string-in-java
                        String category = onePart[0];
                        if (category.equals("Employee")) {
                            importEmployee(allEmployees, onePart, category);
                        } else if (category.equals("Customer")) {
                            importCustomer(allCustomers, onePart, category);
                        } else if (category.equals("Game")) {
                            importGame(listOfItems, onePart, category);
                        } else if (category.equals("Album")) {
                            importAlbum(listOfItems, onePart, category);
                        }
                    } catch (Exception e) {
                        e.getMessage();
                    }
                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public ArrayList<Employee> importEmployee(ArrayList<Employee> allEmployees, String[] onePart, String category) throws Exception {
        int employeeId = Integer.parseInt(onePart[1]);          //Used site to learn String part1 = parts[0]; from https://stackoverflow.com/questions/3481828/how-to-split-a-string-in-java
        String employeeName = onePart[2];
        int employeeBirthYear = Integer.parseInt(onePart[3]);
        String employeeAddress = onePart[4];
        double employeeGrossSalary = Double.parseDouble(onePart[5]);
        System.out.println("Added: <" + category + "> : <" + employeeId + "> : <" + employeeName + "> : <" + employeeBirthYear + "> : <" + employeeAddress + "> : <" + employeeGrossSalary + ">");
        Employee employee = new Employee(employeeId, employeeName, employeeBirthYear, employeeAddress, employeeGrossSalary);
        allEmployees.add(employee);
        return allEmployees;
    }

    public ArrayList<Customer> importCustomer(ArrayList<Customer> allCustomers, String[] onePart, String category){
        int id = Integer.parseInt(onePart[1]);
        String name = onePart[2];
        System.out.println("Added: <" + category + "> : <" + id + "> : <" + name + ">");
        Customer customer = new Customer(id, name);
        allCustomers.add(customer);
        return allCustomers;
    }

    public ArrayList<Item> importGame(ArrayList<Item> listOfItems, String[] onePart, String category) throws Exception {
        int id = Integer.parseInt(onePart[1]);
        String title = onePart[2];
        String genre = onePart[3];
        double rent = Double.parseDouble(onePart[4]);
        String status = "Available";
        double rating = 0.0;
        System.out.println("Added: <" + category + "> : <" + title + "> : <" + genre + "> : <" + rent + ">");
        Game game = new Game(id, title, rent, status, rating, genre);
        listOfItems.add(game);
        return listOfItems;
    }

    public ArrayList<Item> importAlbum(ArrayList<Item> listOfItems, String[] onePart, String category) throws Exception {
        int id = Integer.parseInt(onePart[1]);
        String title = onePart[2];
        String songArtist = onePart[3];
        int songReleaseYear = Integer.parseInt(onePart[4]);
        double rent = Double.parseDouble(onePart[5]);
        String status = "Available";
        double rating = 0.0;
        System.out.println("Added: <" + category + "> : <" + title + "> : <" + songArtist + "> : <" + rent + ">");
        Album album = new Album(id, title, rent, status, rating, songArtist, songReleaseYear);
        listOfItems.add(album);
        return listOfItems;
    }
}
