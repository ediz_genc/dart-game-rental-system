
public class PremiumMember extends Member {

    final int rentLimit = 7;
    final double discount = 0.75;
    final int plusCredits = 5;

    public PremiumMember (int customerId, String customerName) {
        super(customerId, customerName);
    }

    public double calRentCost(int days, double rent) {
        double rentCost;

        if (this.getCredits() < subCredits) {
            rentCost = ((rent * days) * discount);
        }else{
            this.setCredits(getCredits() - subCredits);
            rentCost = 0.0;
        }
        return rentCost;
    }
}

