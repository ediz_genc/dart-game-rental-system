public class DartMain {

    public static void main(String[] args) throws Exception {

        Utility utility = new Utility();
        Controller controller = new Controller();
        Screens screens = new Screens();
        char choiceOfMainMenu;

        System.out.println("Welcome to DART, your good old game rental system. The competition has no steam to keep up! ");
        do {
            System.out.println("1. Enter \"M\" for Manager.");
            System.out.println("2. Enter \"E\" for employee.");
            System.out.println("3. Enter \"C\" for Customer.");
            System.out.println("4. Enter \"X\" to exit system.");

            choiceOfMainMenu = utility.readChar("Please specify your role by entering one of the options given: ");
            choiceOfMainMenu = Character.toUpperCase(choiceOfMainMenu);
            boolean access;
            switch (choiceOfMainMenu) {
                case 'M' -> {
                    access = screens.checkManPass("admin1234");
                    controller.managerIsLoggedIn(access);
                }
                case 'E' -> {
                    access = screens.checkEmpPass("password123");
                    controller.employeeIsLoggedIn(access);
                }
                case 'C' -> {
                    //ask for id and password  (TODO)
                    int customerId = utility.readInt("Type your ID: ");
                    String customerPassword = utility.readString("Type password: ");

                    boolean verified = controller.verifyCustomer(customerId, customerPassword); //check if ID exists in the list of customers, and check if password is correct
                    if (verified) {
                        controller.costumerIsLoggedIn(customerId);
                    } else {
                        System.out.println("Invalid ID or password, please try again!");
                    }
                }
                case 'X' -> {
                    utility.closeScanner();
                    System.out.println();
                    System.out.println("Thank you for using our system.");
                    System.exit(0);
                }
                default -> {
                    System.out.println("Invalid input. Please try again.");
                    main(args);
                }
            }
        } while (choiceOfMainMenu != 'X');
    }
}
	
	