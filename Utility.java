import java.util.Random;
import java.util.Scanner;

public class Utility {

    private Scanner input;

    public Utility(){
        input = new Scanner(System.in);
    }

    public Scanner getInput() {
        return input;
    }

    public void setInput(Scanner input) {
        this.input = input;
    }

    // Generates ID
    // To implement Epic Feature for a VG in Milestone 1, I modified the class "Utility" by
    // adding methods "generateId" & attributes which comes with it and letting the group members
    // to use it for their methods such as game, album and customer Id's. (Ediz Genc)
    public int generateId (){
        Random random = new Random();
        int id = random.nextInt(10000);
        return id;
    }

    //Print methods.
    public String readString(String userMessage) {
        System.out.print(userMessage);
        String textName = input.nextLine();
        return textName;
    }

    public char readChar(String userMessage) {
        System.out.println(userMessage);
        char charText = input.next().charAt(0);
        charText = Character.toUpperCase(charText);
        return charText;
    }

    public double readDouble(String userMessage) {
        System.out.print(userMessage);
        double doubleValue = input.nextDouble();
        input.nextLine();
        return doubleValue;
    }

    public int readInt(String userMessage) {
        System.out.print(userMessage);
        int intValue = input.nextInt();
        input.nextLine();
        return intValue;
    }

    public void closeScanner(){
        input.close();
    }
}
