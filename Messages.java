
public class Messages extends Customer{

    private int senderID;
    private String msg;
    private String messageBeenRead;

    public Messages(int customerId, String customerName, int senderID, String msg, String messageBeenRead) {
        super(customerId, customerName);
        this.senderID = senderID;
        this.msg = msg;
        this.messageBeenRead = messageBeenRead;

    }

    public Messages() {

    }

    public int getSenderID() {
        return senderID;
    }

    public void setSenderID(int senderID) {
        this.senderID = senderID;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMessageBeenRead() {
        return messageBeenRead;
    }

    public void setMessageBeenRead(String messageBeenRead) {
        this.messageBeenRead = messageBeenRead;
    }

    public String ToString(){
        return ("From: <" +  + this.getSenderID() + "> To: <" + this.getCustomerId() + "> Message: <" + this.getMsg() + "> Status: <" + this.getMessageBeenRead() + ">");
    }
}
