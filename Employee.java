import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;

public class Employee {

    // attributes
    private int employeeId;
    private String employeeName;
    private int employeeBirthYear;
    private String employeeAddress;
    private double employeeGrossSalary;
    private int employeeAge;
    private LocalDate birthYear;

    //Constructor
    public Employee(int employeeId, String employeeName, int employeeBirthYear, String employeeAddress, double  employeeGrossSalary) throws Exception {
        if(employeeName.isEmpty()){
            throw new Exception("Invalid data. Employee name cannot be empty.");
        }
        if(employeeGrossSalary < 0){
            throw new Exception("Invalid data. Employee salary cannot be negative.");
        }
        this.employeeId = employeeId;
        this.employeeName = employeeName;
        this.employeeBirthYear = employeeBirthYear;
        this.employeeAddress = employeeAddress;
        this.employeeGrossSalary = employeeGrossSalary;
    }

    public Employee(){

    }

    // getter & setters
    public int getEmployeeId() {
        return this.employeeId;
    }
    public void setEmployeeId(int empId) {
        employeeId = empId;
    }

    public String getEmployeeName() {
        return this.employeeName;
    }
    public void setEmployeeName(String empName) {
        employeeName = empName;
    }

    public int getEmployeeBirthYear() {
        return this.employeeBirthYear;
    }
    public void setEmployeeBirthYear(int empBirthYear) {
        employeeId = empBirthYear;
    }

    public String getEmployeeAddress() {
        return this.employeeAddress;
    }
    public void setEmployeeAddress(String empAddress) {
        employeeAddress = empAddress;
    }

    public double getEmployeeGrossSalary() {
        return this.employeeGrossSalary;
    }
    public void setEmployeeGrossSalary(double empGrossSalary) {
        employeeGrossSalary = empGrossSalary;
    }

    public LocalDate getBirthYear() {
        return birthYear;
    }
    public void setBirthYear(LocalDate birthYear) {
        this.birthYear = birthYear;
    }

    public int getAge(){ // Source Stackoverflow (https://stackoverflow.com/questions/1116123/how-do-i-calculate-someones-age-in-java)
        Calendar today = Calendar.getInstance();
        employeeAge = today.get(Calendar.YEAR) - getEmployeeBirthYear();
        return employeeAge;
    }

    public String toString () {
        return this.getEmployeeId() + " : " + this.getEmployeeName() + " - " + this.getEmployeeBirthYear() + " (" + this.getAge() + ") : " + this.getEmployeeGrossSalary() + " SEK.";
    }

    public ArrayList<Item> addGame(ArrayList<Item> listOfItems, int id, String title, double rent, String genre) throws Exception{

        Game game = new Game(id, title, rent, "Available", 0.0, genre);
        listOfItems.add(game);

        System.out.println("Objects add to the list:");

        viewAllGames(listOfItems);
        System.out.println();
        return listOfItems;
    }

    public void removeGame(ArrayList<Item> listOfItems, int gameToRemove) {

        boolean exist = false;
        for (int i = 0; i < listOfItems.size(); i++) {
            if (gameToRemove == listOfItems.get(i).getId()) {
                listOfItems.remove(i);
                System.out.println("Game with ID <" + gameToRemove + "> has been removed.");
                exist = true;
            }
        }
        if(!listOfItems.contains(gameToRemove) && exist == false){
            System.out.println("Game with ID " + gameToRemove + " not found!");
        }
        if(listOfItems.size() > 0){
            viewAllGames(listOfItems);
        }else{
            System.out.println("All games have been successfully removed.");
        }
        System.out.println();
    }

    public void viewAllGames(ArrayList<Item> listOfItems) {
        System.out.println("List of all games: ");
        for (Item game : listOfItems) {
            if(game instanceof Game){
            System.out.println(game);
            }
        }
    }

    public ArrayList<Customer> addCustomers(ArrayList<Customer> allCustomers, int customerId, String customerName){   //Register customer profile(4.1)

        Customer customer = new Customer(customerId, customerName);
        allCustomers.add(customer);

        System.out.println();
        return allCustomers;
    }

    public void removeCustomer(ArrayList<Customer> allCustomers, int customerIdToRemove) {  //Remove customer profile(4.2)

        int removeCounter = 0;

        System.out.println();

        for (int i = 0; i < allCustomers.size(); i++) {
            if (allCustomers.get(i).getCustomerId() == customerIdToRemove) { //Found in ArrayList
                allCustomers.remove(i);
                System.out.println("Removed customer with ID: " + customerIdToRemove + ".");
            } else if (!allCustomers.contains(customerIdToRemove)) {  //Checks if NOT in ArrayList(allCustomers)
                if (removeCounter < 1) {
                    System.out.println("Customer with id <" + customerIdToRemove + "> not found.");
                    removeCounter++;
                }
            }
        }
        System.out.println();
    }

    public void viewCustomers(ArrayList<Customer> allCustomers) {
        System.out.println("-----Show Customers-----");
        for (Customer customer : allCustomers) {
            System.out.println(customer);
        }
    }

    public ArrayList<Item> addAlbum(ArrayList<Item> listOfSongs, int id, String title, double rent,  String songArtist, int songReleaseYear) throws Exception{
        Album album = new Album(id, title, rent, "Available", 0.0, songArtist, songReleaseYear);
        listOfSongs.add(album);

        return listOfSongs;
    }

    public void removeAlbum(ArrayList<Item> listOfSongs, int songToRemove){

        boolean exists = false;
        for (int i = 0; i < listOfSongs.size(); i++) {
            if (listOfSongs.get(i).getId() == songToRemove) {
                listOfSongs.remove(i);
                System.out.println("Album with ID <" + songToRemove + "> has been removed.");
                exists = true;
            }
        }
        //put else if outside of if, will check if before else if.according to christofer.
        if(!listOfSongs.contains(songToRemove) && exists == false){
            System.out.println("Song with ID <" + songToRemove + "> is not found.");
        }
    }

    public void viewAlbums(ArrayList<Item> listOfItems) {

        for(Item print:listOfItems) {
            if(print instanceof Album){
            System.out.println(print);}
        }
    }


}