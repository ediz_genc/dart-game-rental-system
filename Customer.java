import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


// “To implement Epic Feature 6.3, I modified the class Customer by
// removing attribute daysRented from Customer used to calculate the profit & the calculations
// is done by rent transaction class. (Patricia)
public class Customer {

    private Controller controller;
    private int customerId;
    private String customerName;

    private String password;
    private int itemNo;
    private final int rentLimit = 1;
    private boolean grantedUpgrade;

    public Customer(int customerId, String customerName) {
        this.customerId = customerId;
        this.customerName = customerName;
        this.password = "1234";
        itemNo = 0;
        grantedUpgrade = false;
        this.controller = new Controller();
    }

    public Customer() {

    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getGrantedUpgrade() {
        return grantedUpgrade;
    }

    public void setGrantedUpgrade(boolean grantedUpgrade) {
        this.grantedUpgrade = grantedUpgrade;
    }

    public int getItemNo() {
        return itemNo;
    }

    public void setItemNo(int itemNo) {
        this.itemNo = itemNo;
    }

    public void plusCredit() {
    }

    public String toString() {
        return "<" + this.customerId + "> : <" + this.customerName + ">";
    }

    public void rentItem(ArrayList<Item> listOfItems, int itemChoice) {

        if (rentLimit() == false) {
            System.out.println("You have rented all the items you are allowed to rent, please return an item and try again.");
            return;
        }

        LocalDateTime timeNow = LocalDateTime.now();
        DateTimeFormatter.ofPattern("yyyy/MM/dd");

        for (int i = 0; i < listOfItems.size(); i++) {
            if (itemChoice == listOfItems.get(i).getId()) {
                if (listOfItems.get(i).getStatus().equals("Available")) {
                    listOfItems.get(i).setStatus("Not Available");
                    listOfItems.get(i).setTime(timeNow);
                    System.out.println(listOfItems.get(i) + " has been rented, that will be " + listOfItems.get(i).getRent() + " sek. " + timeNow);
                } else {
                    System.out.println("Item with ID: " + itemChoice + " is already rented");
                }
                return;
            }
        }
        System.out.println("The item with ID " + itemChoice + " not found");
    }


    // “To implement Epic Feature 11, I modified the method returnItem by
    // adding new method storeHistory/ and modifying method returnItem
    // to be able to add information to arrayList allTransactions.” (Patricia)
    public void returnItem(ArrayList<Item> listOfItems, ArrayList<RentTransaction> allTransactions, int itemId, int itemDaysRented){

        Item returnedItem = null;
        for (int i = 0; i < listOfItems.size(); i++) {
            if (itemId == listOfItems.get(i).getId()) {
                if (listOfItems.get(i).getStatus().equals("Not Available")) {
                    returnedItem = listOfItems.get(i);
                    returnedItem.setStatus("Available");
                    double totalItemRentCost = calRentCost(itemDaysRented, listOfItems.get(i).getRent());
                    System.out.println("The price for the rent is: " + totalItemRentCost + " SEK.");
                    allTransactions = storeHistory(allTransactions, customerId, listOfItems.get(i).getId(), totalItemRentCost);
                    this.plusCredit();
                } else {
                    System.out.println("This is item with ID " + itemId + " is available. Please choose another item to return.");
                    return;
                }
            }
        }
        System.out.println("The item with ID " + itemId + " not found");
        controller.rate(returnedItem);
    }

    public void viewAllItems(ArrayList<Item> listOfItems) {
        System.out.println("List of all items: ");
        for (Item item : listOfItems) {
            if (item instanceof Game) {
                System.out.println("Game: " + item);
            } else if (item instanceof Album) {
                System.out.println("Album: " + item);
            }
        }
    }

    public double calRentCost(int days, double rent) {
        double rentCost;
        rentCost = rent * days;
        return rentCost;
    }

    public ArrayList<RentTransaction> storeHistory(ArrayList<RentTransaction> allTransactions, int customerId, int itemId, double totalCost) {

        RentTransaction transaction = new RentTransaction(customerId, itemId, totalCost);
        allTransactions.add(transaction);
        return allTransactions;
    }

    public boolean rentLimit() {  //Epic 8
        boolean rent;

        if (this.itemNo < rentLimit) {
            rent = true;
        } else {
            rent = false;
        }
        return rent;
    }

    public ArrayList<Customer> approvedUpgrade(ArrayList<Customer> allCustomers){  //Epic 8.2

        int id = this.getCustomerId();
        String name = this.getCustomerName();
        SilverMember silver = new SilverMember(id, name);
        silver.setItemNo(this.getItemNo());
        allCustomers.remove(this);
        allCustomers.add(silver);
        return allCustomers;
    }

    public ArrayList<Customer> wantsUpgrade(ArrayList<Customer> listOfUpgrades) { // 8.2

        listOfUpgrades.add(this);

        return listOfUpgrades;
    }


    //  “To implement Epic Feature 10.3, I modified the class "customer" by
//  adding the attributes "sortGame" and "sortAlbum" in methods and the methods "viewSortedBasedOnRating",
//  "sortGame" and "sortAlbum" in customer class. (Ediz Genc)
    public void sortGame(ArrayList<Game> listOfGames) {
        System.out.println("The list of sorted games based on their ratings are as follows; ");
        System.out.println();
        double sortGame = listOfGames.size() - 1;

        for (int i = 0; i < sortGame; i++) {
            double leftGame = sortGame - i;

            for (int j = 0; j < leftGame; j++) {
                if (listOfGames.get(j).getRating() < listOfGames.get(j + 1).getRating()) {
                    swap(listOfGames, j + 1, j);
                }
            }
        }
        for (Game game : listOfGames) {
            System.out.println(game);
        }
    }

    //  “To implement Epic Feature 10.3, I modified the class "customer" by
//  adding the attributes "sortGame" and "sortAlbum" in methods and the methods "viewSortedBasedOnRating",
//  "sortGame" and "sortAlbum" in customer class. (Ediz Genc)
    public void sortAlbum(ArrayList<Album> listOfSongs) {
        System.out.println("The list of sorted albums based on their ratings are as follows; ");
        System.out.println();
        double sortAlbum = listOfSongs.size() - 1;

        for (int i = 0; i < sortAlbum; i++) {
            double leftAlbum = sortAlbum - i;

            for (int j = 0; j < leftAlbum; j++) {
                if (listOfSongs.get(j).getRating() < listOfSongs.get(j + 1).getRating()) {
                    swap(listOfSongs, j + 1, j);
                }
            }
        }
        for (Album song : listOfSongs) {
            System.out.println(song);
        }
    }

    public void swap(List<?> list, int i, int j) { // from collections documentation
        final List itemList = list;
        itemList.set(i, itemList.set(j, itemList.get(i)));
    }

    //  “To implement Epic Feature for a VG in Milestone 2, I modified the class "customer" by
//  adding the attributes "sortAlbumYear" in the method and the method itself "viewSortedBasedOnYear". (Ediz Genc)
    public void viewSortedBasedOnYear(ArrayList<Item> listOfItems) {
        System.out.println("The list of sorted albums based on their release year are as follows; ");
        System.out.println();
        ArrayList<Album> listOfSongs = new ArrayList<>();
        for (Item item : listOfItems) {
            if (item instanceof Album) {
                listOfSongs.add((Album) item);
            }
        }
        double sortAlbumYear = listOfSongs.size() - 1;

        for (int i = 0; i < sortAlbumYear; i++) {
            double leftAlbum = sortAlbumYear - i;

            for (int j = 0; j < leftAlbum; j++) {
                if (listOfSongs.get(j).getSongReleaseYear() < listOfSongs.get(j + 1).getSongReleaseYear()) {
                    swap(listOfSongs, j + 1, j);
                }
            }
        }
        for (Album song : listOfSongs) {
            System.out.println(song);
        }
    }

    public ArrayList<Messages> sendMessage(ArrayList<Customer> allCustomers, ArrayList<Messages> allMessages, int senderId, int idToSendTo, String newMessage) {

        System.out.println("-----Send Message-----");

        boolean exists = false;
        if (senderId == idToSendTo) {
            System.out.println("You cannot send a message to yourself. Please try again.");
        }

        for (int i = 0; i < allCustomers.size(); i++) {
            if (allCustomers.get(i).getCustomerId() == idToSendTo) {
                exists = true;
                System.out.println("Receiver's ID: <" + allCustomers.get(i).getCustomerId() + ">\tReceiver's name: <" + allCustomers.get(i).getCustomerName() + ">");
                String customerName = allCustomers.get(i).getCustomerName();

                String beenRead = "Not Read";
                Messages message = new Messages(senderId, customerName, idToSendTo, newMessage, beenRead);
                allMessages.add(message);
                System.out.println("Message has been sent.");
            }
        }
        if (!allCustomers.contains(idToSendTo) && !exists) {
            System.out.println("A customer with this id does not exist. Please try again.");
            exists = true;
        }
        return allMessages;
    }
}