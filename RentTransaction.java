public class RentTransaction {

    //Attributes
    private int customerId;
    private int itemId;
    private double totalRentCost;  //Used to calculate total rent profit

    public RentTransaction (int customerId, int itemId, double totalCost) {
        this.customerId = customerId;
        this.itemId = itemId;
        this.totalRentCost = totalCost;
    }

    public RentTransaction () {
    }

    public double getTotalRentCost() {
        return totalRentCost;
    }
    public void setTotalRentCost(double totalRentCost) {
        this.totalRentCost = totalRentCost;
    }
    public int getItemId () {
        return itemId;
    }
    public void setItemId (int itemId) {
        this.itemId = itemId;
    }
    public int getCustomerId() {
        return customerId;
    }
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
    @Override
    public String toString() {
        return "RentTransaction{" + "customerId=" + customerId + ", itemId=" + itemId + ", totalRentCost=" + totalRentCost + '}';
    }
}


