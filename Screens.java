import java.util.ArrayList;

public class Screens {

    Utility utility = new Utility();

    public boolean checkManPass(String managerPassword){
        String managerScreenPassword = utility.readString("Type password: ");
        return managerScreenPassword.equals(managerPassword);
    }

    public void managerScreen() {
            System.out.println("Manager Screen - Type one of the options below:");
            System.out.println("1. Add an employee");
            System.out.println("2. Remove Employee");
            System.out.println("3. Employee Salary");
            System.out.println("4. View all employees");
            System.out.println("5. Show most profitable item");
            System.out.println("6. Show rent frequency");
            System.out.println("7. Show best customer");
            System.out.println("8. Show total rent profit");
            System.out.println("9. Import from a file");
            System.out.println("10. Return to Main Menu");
    }

    public boolean checkEmpPass(String EmployeePassword){
        String employeeScreenPassword = utility.readString("Type password: ");
        return employeeScreenPassword.equals(EmployeePassword);
    }

    public void employeeScreen () {
        System.out.println("Employee Screen - Type one of the options below: ");
        System.out.println("1. Register a game");
        System.out.println("2. Remove a game");
        System.out.println("3. Register a customer");
        System.out.println("4. Remove a customer");
        System.out.println("5. Register an album");
        System.out.println("6. Remove an album");
        System.out.println("7. View all games");
        System.out.println("8. View all customers");
        System.out.println("9. View all albums");
        System.out.println("10. View & upgrade customers");
        System.out.println("11. Return to Main Menu");
    }

    public void customerScreen() {
        System.out.println("Customer Screen - Type one of the options below: ");
        System.out.println("1. Rent an item");
        System.out.println("2. Return an item");
        System.out.println("3. Compose a message");
        System.out.println("4. Inbox");
        System.out.println("5. Sent messages");
        System.out.println("6. Search an item");
        System.out.println("7. View items based on their release year");
        System.out.println("8. View items based on average ratings");
        System.out.println("9. Change password"); //change password option
        System.out.println("10. Apply for membership upgrade");
        System.out.println("11. Return to Main Menu");
    }

    public void printCustomers(ArrayList<Customer> allCustomers){
        for (Customer cu : allCustomers){
            System.out.println(cu.toString());

        }
    }

    public void printReceivedMessages(ArrayList<Messages> allMessages, int yourID){
        System.out.println("-----Your Messages-----");
        for (Messages messages : allMessages){
            if (messages.getCustomerId() == yourID && messages.getMessageBeenRead().equals("Not Read")) {
                messages.setMessageBeenRead("Read");
                System.out.println(messages.toString());
            }

        }
    }

    public void printAllMessages(ArrayList<Messages> allMessages, int yourID){
        System.out.println("-----All Messages-----");
        for (Messages messages : allMessages){
            if (messages.getCustomerId() == yourID){
                System.out.println(messages.toString());
            }
        }
    }


    public void sentMessages(ArrayList<Messages> allMessages, int yourID){
        for (Messages messages : allMessages){
            if (messages.getSenderID() == yourID){
                System.out.println(messages.toString());
            }
        }
    }
}




